﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

    public AudioSource audSrc;
    private bool muted = false;
    private static AudioManager instance;

	// Use this for initialization
	void Start () {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            GameObject.Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
        if (!audSrc.isPlaying)
        {
            audSrc.Play();
        }
	}

    void OnLevelWasLoaded()
    {
        //foreach (AudioManager audMan in FindObjectsOfType<AudioManager>())
        //{
        //    if (audMan != this && audMan != null)
        //    {
        //        GameObject.Destroy(this.gameObject);
        //    }
        //}
    }

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.M))
        {
            SwitchMute();   
        }

        if (!muted && !audSrc.isPlaying)
        {
            audSrc.Play();
        }
        else if(muted && audSrc.isPlaying)
        {
            audSrc.Stop();
        }

	}

    private void SwitchMute()
    {
        Debug.Log("Mute toggled");
        muted = !muted;
    }
}
