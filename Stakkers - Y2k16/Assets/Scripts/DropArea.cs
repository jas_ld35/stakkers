﻿using UnityEngine;
using System.Collections;

public class DropArea : MonoBehaviour {

    public int numberNeededToLose = 3;
    public static bool playerLost = false;
    public ScoreManager sm;
    private LevelManager lm; 

    int count = 0;

    void Start()
    {
        playerLost = false;
    
        lm = GameObject.Find("_MANAGER").GetComponent<LevelManager>();

        if (PlayerPrefs.GetInt("Complexity") <= 3)
        {
            numberNeededToLose = 1;
        }
    }

    void Update ()
    {
        if (count >= numberNeededToLose)
        {
            playerLost = true;
            lm.OnLose();
        }
    }

	void OnTriggerEnter(Collider col)
    {
        if (col.CompareTag("Player"))
        {
            count++;
            sm.ScorePenalty();
        }
    }
}
