﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenuStart : MonoBehaviour
{
    public GameObject credits;

    void Start()
    {
        credits.SetActive(false);
    }

    public void ToggleCredits()
    {
        bool currentState = credits.active;

        credits.SetActive(!currentState);
    }

    public void LoadLevel(int chosenLevel)
    {
        if (chosenLevel == 1)
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.SetInt("Current Level", 1);
            PlayerPrefs.SetInt("Complexity", 1);


            PlayerPrefs.SetInt("WallX", 5);
            PlayerPrefs.SetInt("WallY", 3);

            SceneManager.LoadScene(chosenLevel);
        }
        if (chosenLevel == 2)
        {
            SceneManager.LoadScene(chosenLevel-1);
        }
        if (chosenLevel == 3)
        {
            Application.Quit();
        }
    }
}
