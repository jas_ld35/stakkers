﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    [SerializeField]
    private int currentLevel = 1;
    private int currentComplexity;
    private Vector2 currentWallSize;
    private int noOfShapes;
    private WallGenerator wallGen;
    private int maxShapes = 8;
    private float finishTimer;

    public int startingComplexity = 1;
    public Vector2 startingWallSize;
    public Vector2 maxSize;
    public int startNoOfShapes = 1;
    public float spawnAreaSize = 35f;
    public static bool allowBlockManipulation = true;
    public static bool levelOver = false;
    public static bool wallFinished = false;

    public GameObject wallFragment;
    public List<GameObject> stakkable;
    public List<GameObject> nonstakkable;
    public GameObject playerShape;
    public Transform spawnPos;

    public static Vector2 mouseClamp;

    public UIManager uiManager;
    public Transform wallGhostPosition;
    private GameObject wallClone;

    void Awake()
    {
        levelOver = false;
        currentComplexity = startingComplexity;
        currentWallSize = startingWallSize;
        noOfShapes = startNoOfShapes;
        allowBlockManipulation = true;
        wallFinished = false;

        LoadPrefs();
        Debug.Log(currentWallSize);
        CalculateValues();
        SpawnShapes();
        CreateWall();
        
    }

    void Update()
    {
        if (DropArea.playerLost == false)
        {
            if (wallFinished)
            {
                finishTimer += Time.deltaTime;

                if (finishTimer >= 2f)
                {
                    OnVictory();
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    void CreateWall()
    {
        wallGen = new WallGenerator((int)currentWallSize.x, (int)currentWallSize.y, GameObject.Find("_Wall"), currentComplexity, wallFragment, stakkable, nonstakkable);
        wallGen.GenerateWall();
        wallClone = wallGen.GenerateWallCopy(wallGhostPosition.position);

        mouseClamp.x = -0.75f - (currentWallSize.x / 2f) * wallFragment.transform.localScale.x;
        mouseClamp.y = -0.75f + (currentWallSize.x / 2f) * wallFragment.transform.localScale.x;
    }

    void SpawnShapes()
    {
        float temp = spawnAreaSize / noOfShapes;

        for (int index = 0; index < noOfShapes; index++)
        {
            GameObject shape = GameObject.Instantiate(playerShape);

            shape.transform.position = new Vector3(spawnPos.transform.position.x - (spawnAreaSize / 2f) + index * temp, shape.GetComponent<Collider>().bounds.size.y, spawnPos.transform.position.z);
           
        }
    }

    void CalculateValues()
    {
        if (currentLevel % 2 == 0)
        {
            currentComplexity++;
        }

        //Hi Sam :)
        if (noOfShapes < maxShapes)
            noOfShapes = currentComplexity;

        if (noOfShapes == 0)
        {
            noOfShapes = 1;
        }

        if (currentComplexity % 3 == 0)
        {
            if (currentWallSize.x <= maxSize.x - 2)
            {
                currentWallSize.x += 2;
            }

            if (currentWallSize.y < maxSize.y)
            {
                currentWallSize.y++;
            }
        }
    }

    void SavePrefs()
    {
        PlayerPrefs.SetInt("Current Level", currentLevel);
        PlayerPrefs.SetInt("Complexity", currentComplexity);
        PlayerPrefs.SetInt("Shapes", noOfShapes);
        PlayerPrefs.SetInt("WallX", (int)currentWallSize.x);
        PlayerPrefs.SetInt("WallY", (int)currentWallSize.y);
    }

    void LoadPrefs()
    {
        currentLevel = PlayerPrefs.GetInt("Current Level");
        currentComplexity = PlayerPrefs.GetInt("Complexity");
        noOfShapes = PlayerPrefs.GetInt("Shapes");
        currentWallSize.x = PlayerPrefs.GetInt("WallX");
        currentWallSize.y = PlayerPrefs.GetInt("WallY");
        Debug.Log("" + currentWallSize.x + " " + currentWallSize.y);
    }

    //When we win, we want to move onto the new level and save what the current level is.
    public void OnVictory()
    {
        currentLevel++;
        levelOver = true;
        SavePrefs();
        uiManager.ActiveGameMenu(true, ScoreManager.currentScore);
    }

    //When we lose, we just want to reload the current level
    public void OnLose()
    {
        levelOver = true;
        uiManager.ActiveGameMenu(false, ScoreManager.currentScore);
    }

    public void MenuButtonPress(int index)
    {
        if (DropArea.playerLost == false)
        {
            int temp = PlayerPrefs.GetInt("Total Score");
            temp += ScoreManager.currentScore;
            PlayerPrefs.SetInt("Total Score", temp);
        }

        SceneManager.LoadScene(index);
    }
}
