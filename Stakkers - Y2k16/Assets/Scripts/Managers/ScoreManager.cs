﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour
{

    public int startScore = 10000;
    public int scoreLose = 50;

    public Text levelScore;
    public Text totalScore;

    public static int currentScore;
    private int timePen;
    float timer = 0;

    public bool updateScoring = true;
    public static bool wallMoving = false;
    public bool updatedTotalScore;

    // Use this for initialization
    void Awake()
    {
        wallMoving = false;
        updateScoring = true;
        updatedTotalScore = false;
        timer = 0f;

        currentScore = startScore * PlayerPrefs.GetInt("Complexity");
        timePen = scoreLose * PlayerPrefs.GetInt("Complexity");
        totalScore.text = PlayerPrefs.GetInt("Total Score").ToString();
    }

    // Update is called once per frame
    void Update()
    {

        levelScore.text = currentScore.ToString();

        if (updateScoring)
        {
            timer += Time.deltaTime;

            if (timer >= 1f)
            {
                currentScore -= timePen;
                timer = 0f;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) || wallMoving)
        {
            updateScoring = false;
        }

        //if (LevelManager.levelOver && !updatedTotalScore)
        //{
        //    int temp = PlayerPrefs.GetInt("Total Score");
        //    temp += currentScore;
        //    PlayerPrefs.SetInt("Total Score", temp);
        //    updatedTotalScore = true;
        //}
    }

    public void ScorePenalty()
    {
        currentScore -= timePen * 5;
    }
}