﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManager : MonoBehaviour {

    [Header("Menu")]
    public GameObject menuObject;
    public Text resultText;
    public Text resultScore;
    public Text levelButtonText;

    [Header("Score")]
    public Text totalScore;
    public Text currentScore;

    [Header("Misc.")]
    public Text countdownText;

	// Use this for initialization
	void Start () {
        menuObject.SetActive(false);
	}
	
	public void ActiveGameMenu(bool result, int score) {
        menuObject.SetActive(true);

        if (result)
        {
            resultText.text = "You Win!";
            levelButtonText.text = "Next Level";
        } 
        else
        {
            resultText.text = "You Lose!";
            levelButtonText.text = "Retry Level";
        }

        resultScore.text = "You scored: \n\n" + score.ToString();
    }
}
