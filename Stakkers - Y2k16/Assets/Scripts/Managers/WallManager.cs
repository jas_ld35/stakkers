﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WallManager : MonoBehaviour {

    public Transform wallStartPosition;
    public Transform wallEndPosition;
    public GameObject wallGO;

    public float wallTravelTime = 10f;
    public float wallStartDelay = 30f;

    public Text ui_timerText;

    private Transform m_transform;
    private float timer;


	// Use this for initialization
	void Start () {
        m_transform = wallGO.transform;
        m_transform.position = wallStartPosition.position;

        timer = wallStartDelay;
	}
	
	// Update is called once per frame
	void Update () {

        if (timer > 0)
        {
            ui_timerText.text = ((int)timer).ToString();
        }
        else
        {
            ui_timerText.text = "GO!";
        }

        timer -= Time.deltaTime;

        if (timer <= 0f)
        {
            timer = 0f;
            //Lerp Position
            StartCoroutine("WallMovement");
            ScoreManager.wallMoving = true;
            LevelManager.allowBlockManipulation = false;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            timer = 0f;
        }

    }

    IEnumerator WallMovement()
    {
        float moveTimer = 0;
        float interp = 0;

        while (interp <= 1)
        {
            moveTimer += Time.deltaTime;
            interp = moveTimer / wallTravelTime;

            m_transform.position = Vector3.Lerp(wallStartPosition.position, wallEndPosition.position, interp);
            yield return null;
        }
        LevelManager.wallFinished = true;
    }
}
