﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OnClickShapeChange : MonoBehaviour
{
    public Mesh[] testMesh;
    int currentMesh = 0;

    private Rigidbody m_rigidbody;
    private Transform m_transform;
    private Vector3 screenPoint;
    private Vector3 offset;
    private bool mouseHeld = false;

    void Start()
    {
        m_rigidbody = GetComponent<Rigidbody>();
        m_transform = transform;

        int randMesh = Random.Range(0, testMesh.Length);
        currentMesh = randMesh;
        gameObject.GetComponent<MeshFilter>().sharedMesh = testMesh[randMesh];
        gameObject.GetComponent<MeshCollider>().sharedMesh = testMesh[randMesh];

    }

    void Update()
    {

        if (LevelManager.allowBlockManipulation == false)
        {
            mouseHeld = false;
        }

        ClampObjectPosition();
    }

    void ClampObjectPosition()
    {
        if (m_transform.position.x < LevelManager.mouseClamp.x)
        {
            m_transform.position = new Vector3(LevelManager.mouseClamp.x + 0.1f, m_transform.position.y, m_transform.position.z);
        }

        else if (m_transform.position.x > LevelManager.mouseClamp.y)
        {
            m_transform.position = new Vector3(LevelManager.mouseClamp.y - 0.1f, m_transform.position.y, m_transform.position.z);
        }

        if (mouseHeld)
        {
            if (m_transform.position.y < -0.5f)
            {
                m_transform.position = new Vector3(m_transform.position.x, -0.2f + 0.15f, m_transform.position.z);
            }
        }
    }

    void OnMouseOver()
    {
        if (LevelManager.allowBlockManipulation)
        {
            if (Input.GetMouseButtonDown(1))
            {
                gameObject.transform.position = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);

                currentMesh++;

                if (currentMesh < testMesh.Length)
                {
                    gameObject.GetComponent<MeshFilter>().sharedMesh = testMesh[currentMesh];
                    gameObject.GetComponent<MeshCollider>().sharedMesh = testMesh[currentMesh];
                }
                else
                {
                    currentMesh = 0;
                    gameObject.GetComponent<MeshFilter>().sharedMesh = testMesh[currentMesh];
                    gameObject.GetComponent<MeshCollider>().sharedMesh = testMesh[currentMesh];
                }
            }

            screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);

            offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

        }
    }

    void OnMouseDrag()
    {
        if (LevelManager.allowBlockManipulation)
        {
            Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

            Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
            m_rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            transform.position = curPosition;
            mouseHeld = true;
        }
    }

    void OnMouseUp()
    {
        m_rigidbody.constraints = RigidbodyConstraints.None;
        m_rigidbody.velocity = Vector3.zero;
        mouseHeld = false;
    }

}


