﻿using UnityEngine;
using System.Collections;

public class PyrSpinner : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(Vector3.forward * (Time.deltaTime * 50));
    }
}
