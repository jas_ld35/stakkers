﻿using UnityEngine;
using System.Collections;

public class SunRotation : MonoBehaviour
{
    bool clockwise = false;

   void Update()
    {
        if(gameObject.transform.rotation.x < (5 * Mathf.Deg2Rad) && clockwise!= true)
        {
            clockwise = true;
            Debug.Log("moving clockwise");
        }
        if(gameObject.transform.rotation.x > (40 * Mathf.Deg2Rad) && clockwise != false)
        {

            clockwise = false;
            Debug.Log("not moving clockwise");
        }

        if (clockwise == true)
        {
            this.transform.Rotate(Time.deltaTime * 10, 0, 0);
        }
        if(clockwise == false)
        {
            this.transform.Rotate(-Time.deltaTime * 10, 0, 0);
        }
    }
}
