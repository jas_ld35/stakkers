﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class WallGenerator
{

    private int xSize;
    private int ySize;
    private Vector3 wallPos;
    private GameObject[,] wall;
    private int complexity;
    private GameObject wallElement;
    private GameObject parent;
    private float wallWidth;

    private int inverseVariety = 20; //lower = more variety, longer loading

    private List<GameObject> stackables;
    private List<GameObject> unstackables;

    public WallGenerator(int x, int y, GameObject parent, int complexity, GameObject wallFragment, List<GameObject> stackableGO, List<GameObject> unstackableGO)
    {
        xSize = x;
        ySize = y;
        this.wallPos = parent.transform.position;
        this.complexity = complexity;
        wallElement = wallFragment;
        wall = new GameObject[x, y];
        this.parent = parent;
        wallWidth = xSize * wallFragment.transform.localScale.x;
        stackables = stackableGO;
        unstackables = unstackableGO;
    }

    public void GenerateWall()
    {
        GameObject newElement;

        System.Random rand = new System.Random(Guid.NewGuid().GetHashCode());

        for (int xCount = 0; xCount < xSize; xCount++)
        {
            for (int yCount = 0; yCount < ySize; yCount++)
            {

                newElement = GameObject.Instantiate(wallElement); //GameObject.CreatePrimitive(PrimitiveType.Cube);
                newElement.transform.SetParent(parent.transform);
                newElement.transform.position = new Vector3(xCount * newElement.transform.localScale.x - wallWidth / 2f, yCount * newElement.transform.localScale.y, parent.transform.position.z);
                Rigidbody e_rigidbody = newElement.AddComponent<Rigidbody>();
                e_rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
                e_rigidbody.useGravity = false;
                e_rigidbody.mass = 3.0f;
                e_rigidbody.isKinematic = true;
                e_rigidbody.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

                newElement.name = "WallElement " + xCount + " " + yCount;

                WallTag tag = newElement.AddComponent<WallTag>();
                tag.Position = new Vector2(xCount, yCount);
                wall[xCount, yCount] = newElement;

            }
        }
        GenerateWallGaps();
    }

    public GameObject GenerateWallCopy( Vector3 targetPos)
    {
        GameObject copy = GameObject.Instantiate(parent);

        copy.transform.position = new Vector3(copy.transform.position.x, copy.transform.position.y, targetPos.z);

        Material oldMat = null;

        foreach (GameObject obj in wall)
        {
            if (obj != null && obj.name.Contains("WallElement"))
            {
                oldMat = new Material(obj.GetComponent<Renderer>().material);
                break;
            }
        }
        
        foreach (GameObject obj in wall) {
        
            if (obj != null) {
                GameObject.Destroy(obj.GetComponent<Collider>());
            }
        }

        oldMat.color = new Color(1f, 1f, 1f, 0.0025f);

        foreach (Renderer childRend in copy.GetComponentsInChildren<Renderer>())
        {
            childRend.material = oldMat;
        }

        return copy;
    }

    private void GenerateWallGaps()
    {
        Queue<GameObject> potentialGaps = new Queue<GameObject>();
        List<GameObject> processedElements = new List<GameObject>();
        List<GameObject> toBeRemoved = new List<GameObject>();

        potentialGaps.Enqueue(wall[xSize / 2, 0]);
        potentialGaps.Enqueue(wall[xSize / 4, 0]);
        potentialGaps.Enqueue(wall[(xSize / 4) * 3, 0]);

        double chance;
        double threshold = 0.0;

        System.Random rand = new System.Random();
        GameObject tempGameObj;

        while (toBeRemoved.Count < complexity) //until the wall has enough elements removed
        {
            if (potentialGaps.Count == 0) //run out of things to remove
            {
                threshold = 0.0;

                foreach (GameObject element in processedElements)
                {
                    potentialGaps.Enqueue(element);
                }

                processedElements = new List<GameObject>();
            }
            chance = rand.NextDouble();

            tempGameObj = potentialGaps.Dequeue();
            processedElements.Add(tempGameObj);

            if (chance > threshold)
            {
                if (!(toBeRemoved.Contains(tempGameObj)))
                {

                    toBeRemoved.Add(tempGameObj);

                    //If it's being removed then its neighbours also have a chance of being removed
                    foreach (GameObject neighbour in GetNeighbours(tempGameObj, true))
                    {
                        if (!(processedElements.Contains(neighbour))) //if not already processed
                        {
                            potentialGaps.Enqueue(neighbour); //put it in the queue to be processed
                        }
                    }
                }
            }

            threshold = Sigmoid(toBeRemoved.Count / (inverseVariety * (1 + toBeRemoved.Count / 2)));
        }

        //free some memory up
        processedElements = null;

        FillGapsWithHoles(toBeRemoved);
    }

    private void FillGapsWithHoles(List<GameObject> gaps)
    {
        List<WallTag> gapTags = new List<WallTag>(gaps.Count);


        List<int> xValues = new List<int>();

        foreach (GameObject obj in gaps)
        {
            WallTag tag = obj.GetComponent<WallTag>();
            if (!(xValues.Contains((int)tag.Position.x)))
            {
                xValues.Add((int)tag.Position.x);
            }
            gapTags.Add(tag);
        }

        int[] yValues = new int[xValues.Count];

        int count = 0;

        int yMax = 0;
        foreach (int xVal in xValues)
        {
            foreach (WallTag tag in gapTags)
            {
                if (tag.Position.x == xVal)
                {
                    if (tag.Position.y > yMax)
                    {
                        yMax = (int)tag.Position.y;
                    }
                }
            }

            yValues[count] = yMax;
            count++;
            yMax = 0;
        }

        RemoveElements(gaps);

        foreach (WallTag wallTag in gapTags)
        {
            FillGap(wallTag, xValues.ToArray(), yValues);
        }
    }

    private void FillGap(WallTag tag, int[] xVals, int[] yVals)
    {
        #region OldCode
        //System.Random rand = new System.Random();

        //GameObject newElement = GameObject.Instantiate(stackables[rand.Next(0, stackables.Count)]);//default

        //GameObject wall_open_ns_GO = null;
        //GameObject wall_open_sside_GO = null;
        //GameObject wall_open_NEW = null;

        //#region FindGapModels
        //foreach (GameObject stackableObj in stackables)
        //{
        //    if (stackableObj.name == "Wall_Open_NS")
        //    {
        //        wall_open_ns_GO = stackableObj;
        //    }
        //    else if (stackableObj.name == "Wall_Open_SSide")
        //    {
        //        wall_open_sside_GO = stackableObj;
        //    }
        //    else if (stackableObj.name == "Wall_OpenNEW")
        //    {
        //        wall_open_NEW = stackableObj;
        //    }
        //}

        //foreach (GameObject unstackableObj in unstackables)
        //{
        //    if (unstackableObj.name == "Wall_Open_NS")
        //    {
        //        wall_open_ns_GO = unstackableObj;
        //    }
        //    else if (unstackableObj.name == "Wall_Open_SSide")
        //    {
        //        wall_open_sside_GO = unstackableObj;
        //    }
        //    else if (unstackableObj.name == "Wall_OpenNEW")
        //    {
        //        wall_open_NEW = unstackableObj;
        //    }
        //}
        //#endregion

        //int[] tagMap = GetEmptyNeighbourMap(tag);
        //int top = tagMap[0];
        //int left = tagMap[1];
        //int right = tagMap[2];
        //int bottom = tagMap[3];

        //Debug.Log("(" + tag.Position.x + "," + tag.Position.y + ") - map: [" + tagMap[0] + "," + tagMap[1] + "," + tagMap[2] + "," + tagMap[3] + "]");

        ////unstackables
        //for (int counter = 0; counter < xVals.Length; counter++)
        //{
        //    if (tag.Position.x == xVals[counter] && tag.Position.y == yVals[counter])
        //    {
        //        if (left == 0 && right == 0)//empty to left and right 
        //        {
        //            newElement = wall_open_NEW;
        //            //Debug.Log("wall_openNEW");
        //        }
        //        else
        //        {
        //            if (left == 1 && right == 1) //blocks on both sides
        //            {
        //                newElement = GameObject.Instantiate(unstackables[rand.Next(0, unstackables.Count)]);
        //                //Debug.Log("Block on both sides, random");
        //            }
        //            else
        //            {
        //                newElement = GameObject.Instantiate(wall_open_sside_GO);
        //                //Debug.Log("wall_open_sside_GO");
        //                if (left == 1) //block on left side
        //                {
        //                    newElement.transform.Rotate(new Vector3(0, 0, 1), -90); //turn it to face the right way
        //                    Debug.Log("...Rotated");
        //                }
        //                else if (right == 1) //block on right side
        //                {
        //                    //don't rotate
        //                }
        //            }
        //        }
        //        newElement.name += " (" + tag.Position.x + "," + tag.Position.y + ")";
        //        newElement.transform.position = new Vector3(xVals[counter] * newElement.transform.localScale.x - wallWidth / 2f, yVals[counter] * newElement.transform.localScale.y, parent.transform.position.z);
        //        newElement.transform.parent = parent.transform;
        //        wall[xVals[counter], yVals[counter]] = newElement;
        //        return; //top position filled as required so return 
        //    }
        //}



        ////stackables
        //if (left == 1 || right == 1 || top == 1 || bottom == 1)
        //{
        //    newElement = GameObject.Instantiate(stackables[rand.Next(0, stackables.Count)]);
        //    newElement.name += " (" + tag.Position.x + "," + tag.Position.y + ")";
        //    newElement.transform.position = new Vector3(tag.Position.x * newElement.transform.localScale.x - wallWidth / 2f, tag.Position.y * newElement.transform.localScale.y, parent.transform.position.z);
        //    newElement.transform.parent = parent.transform;
        //    wall[(int)tag.Position.x, (int)tag.Position.y] = newElement;
        //}
        //else //no surrounding block so don't fill
        //{
        //    Debug.Log("...Not spawning anything");
        //    return;
        //}
        #endregion

        #region NewCode
        System.Random rand = new System.Random(Guid.NewGuid().GetHashCode());
        for (int counter = 0; counter < xVals.Length; counter++)
        {
            if (tag.Position.x == xVals[counter] && tag.Position.y == yVals[counter])
            {
                if (tag.Position.y != 0 && !(rand.Next(0, 101) % 4 == 0))
                {
                    GameObject newElement = GameObject.Instantiate(unstackables[rand.Next(0, unstackables.Count)]);
                    newElement.transform.parent = parent.transform;
                    newElement.name = "Gap (" + tag.Position.x + "," + tag.Position.y + ")";
                    newElement.transform.position = new Vector3(tag.Position.x * newElement.transform.localScale.x - wallWidth / 2f, tag.Position.y * newElement.transform.localScale.y, parent.transform.position.z);
                    Rigidbody wrb = newElement.AddComponent<Rigidbody>();

                    wrb.isKinematic = true;
                    wrb.useGravity = false;
                    wrb.mass = 3f;
                    wrb.constraints = RigidbodyConstraints.FreezeRotation;
                    wrb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

                    wall[(int)tag.Position.x, (int)tag.Position.y] = newElement;
                }
            }
        }
        #endregion
    }

    /// <summary>
    /// Get the map of surrounding blocks
    /// </summary>
    /// <param name="tag"></param>
    /// <returns>int[] holding -1,0 or 1. -1 means there is no block, 0 means a gap and 1 means a wall element. Order: top, left, right, down</returns>
    private int[] GetEmptyNeighbourMap(WallTag tag)
    {
        int[] map = new int[4];

        #region BelowBlock
        if (tag.Position.y == 0) //nothing below as at the bottom of the wall
        {
            map[3] = -1;
        }
        else
        {
            GameObject belowBlock = wall[(int)tag.Position.x, (int)tag.Position.y - 1];
            if (belowBlock != null) // something below
            {
                if (belowBlock.name.Contains("WallElement")) //it's a block below
                {
                    map[3] = 1;
                }
                else //it's a gap below
                {
                    map[3] = 0;
                }
            }
            else //nothing below
            {
                map[3] = 0;
            }
        }
        #endregion

        #region AboveBlock
        if (tag.Position.y == ySize - 1) //nothing above as at top of wall
        {
            map[0] = -1;
        }
        else
        {
            GameObject aboveBlock = wall[(int)tag.Position.x, (int)tag.Position.y + 1];
            if (aboveBlock != null) // something above
            {
                if (aboveBlock.name.Contains("WallElement")) //it's a block above
                {
                    map[0] = 1;
                }
                else //it's a gap above
                {
                    map[0] = 0;
                }
            }
            else //nothing above
            {
                map[0] = 0;
            }
        }
        #endregion

        #region LeftBlock
        if (tag.Position.x == 0) //nothing left as at side of wall
        {
            map[1] = -1;
        }
        else
        {
            GameObject leftBlock = wall[(int)tag.Position.x - 1, (int)tag.Position.y];
            if (leftBlock != null) // something above
            {
                if (leftBlock.name.Contains("WallElement")) //it's a block left
                {
                    Debug.Log("Left name (" + leftBlock.name + ") contains WallElement");
                    map[1] = 1;
                }
                else //it's a gap left
                {
                    map[1] = 0;
                }
            }
            else //nothing left
            {
                map[1] = 0;
            }
        }
        #endregion

        #region RightBlock
        if (tag.Position.x == xSize - 1) //nothing right as at side of wall
        {
            map[2] = -1;
        }
        else
        {
            GameObject rightBlock = wall[(int)tag.Position.x + 1, (int)tag.Position.y];
            if (rightBlock != null) // something right
            {
                if (rightBlock.name.Contains("WallElement")) //it's a block right
                {
                    map[2] = 1;
                }
                else //it's a gap right
                {
                    map[2] = 0;
                }
            }
            else //nothing right
            {
                map[2] = 0;
            }
        }
        #endregion

        return map;
    }

    private double Sigmoid(double val)
    {
        return 1 / (1 + Math.Exp(-val));
    }

    private void RemoveElements(List<GameObject> removees)
    {
        //for (int xCount = 0; xCount < xSize; xCount++)
        //{
        //    for (int yCount = 0; yCount < ySize; yCount++)
        //    {
        //        if (removees.Contains(wall[xCount, yCount]))
        //        {
        //            GameObject.Destroy(wall[xCount, yCount]);
        //        }
        //    }
        //}
        foreach (GameObject obj in removees)
        {
            GameObject.Destroy(obj);
        }
    }

    private void RemoveElement(GameObject removee)
    {
        for (int xCount = 0; xCount < xSize; xCount++)
        {
            for (int yCount = 0; yCount < ySize; yCount++)
            {
                if (wall[xCount, yCount] == removee)
                {
                    GameObject.Destroy(removee);
                    break;
                }
            }
        }
    }

    private List<GameObject> GetNeighbours(GameObject wallElement, bool validGapNeighboursOnly)
    {
        List<GameObject> neighbours = new List<GameObject>(4);
        WallTag tag;
        tag = wallElement.GetComponent<WallTag>();

        if (tag == null)
        {
            throw new Exception("Can't have a wall element without a WallTag");
        }

        if (tag.Position.x != 0)
        {
            if (validGapNeighboursOnly)
            {
                if (tag.Position.x != 1 && tag.Position.y != 0)
                {
                    if (wall[(int)tag.Position.x - 2, (int)tag.Position.y - 1] == null)
                    {
                        neighbours.Add(wall[(int)tag.Position.x - 1, (int)tag.Position.y]);
                    }
                }
                else
                {
                    neighbours.Add(wall[(int)tag.Position.x - 1, (int)tag.Position.y]);
                }
            }
            else
            {
                neighbours.Add(wall[(int)tag.Position.x - 1, (int)tag.Position.y]);
            }
        }


        if (tag.Position.y != 0)
        {
            if (validGapNeighboursOnly)
            {
                if (tag.Position.y != 1)
                {
                    if (wall[(int)tag.Position.x, (int)tag.Position.y - 2] == null)
                    {
                        neighbours.Add(wall[(int)tag.Position.x, (int)tag.Position.y - 1]);
                    }
                }
                else
                {
                    neighbours.Add(wall[(int)tag.Position.x, (int)tag.Position.y - 1]);
                }
            }
            else
            {
                neighbours.Add(wall[(int)tag.Position.x, (int)tag.Position.y - 1]);
            }
        }

        if (!(tag.Position.x >= xSize - 1))
        {
            if (validGapNeighboursOnly)
            {
                if (tag.Position.y != 0)
                {
                    if (wall[(int)tag.Position.x + 1, (int)tag.Position.y - 1] == null)
                    {
                        neighbours.Add(wall[(int)tag.Position.x + 1, (int)tag.Position.y]);
                    }
                }
                else
                {

                }
            }
            else
            {
                neighbours.Add(wall[(int)tag.Position.x + 1, (int)tag.Position.y]);
            }
        }

        if (!(tag.Position.y >= ySize - 1))
        {
            neighbours.Add(wall[(int)tag.Position.x, (int)tag.Position.y + 1]);
        }

        return neighbours;
    }

}
