﻿using UnityEngine;
using System.Collections;

public class WallTag : MonoBehaviour {

    public Vector2 Position { get; set; }

    public WallTag(Vector2 position)
    {
        Position = position;
    }

}
